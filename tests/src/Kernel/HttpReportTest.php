<?php

declare(strict_types=1);

namespace Drupal\Tests\archimedes_client\Kernel;

use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\Psr7\Response;

/**
 * HttpReportTest.
 *
 * Tests for a Mataara Report being sent over HTTP.
 */
class HttpReportTest extends ReportTestBase {

  /**
   * Report service.
   *
   * @var \Drupal\archimedes_client\Report
   */
  protected $report;

  /**
   * History of requests/responses.
   *
   * @var array
   */
  protected $history = [];

  /**
   * Mock client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $mockClient;

  /**
   * Mocks the http-client.
   */
  protected function mockClient(Response ...$responses) : void {
    if (!isset($this->mockClient)) {
      $mock = new MockHandler($responses);

      $handler_stack = HandlerStack::create($mock);
      $history = Middleware::history($this->history);
      $handler_stack->push($history);
      $this->mockClient = new Client(['handler' => $handler_stack]);
    }
    $this->container->set('http_client', $this->mockClient);
    $this->report = \Drupal::service('archimedes_client.report');
    $this->report->invalidateCachedData();
  }

  /**
   * Tests HTTP report submission using mocked HTTP.
   */
  public function testHttpReport(): void {
    $this->mockClient(new Response(200, [], '{"success": true}'));
    $this->report = \Drupal::service('archimedes_client.report');
    $this->report->invalidateCachedData();
    $result = $this->report->sendHttp();
    $this->assertTrue($result);
  }

}
