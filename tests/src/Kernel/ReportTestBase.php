<?php

declare(strict_types=1);

namespace Drupal\Tests\archimedes_client\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * ReportTestBase.
 *
 * Abstract class to use for Report testing in Kernel tests.
 */
abstract class ReportTestBase extends KernelTestBase {

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = [
    'archimedes_client',
    'system',
    'user',
  ];

  /**
   * Themes to install.
   *
   * @var array
   */
  protected static $themes = [
    'stark',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Private encryption report key to use for testing.
   *
   * @var string
   */
  protected $testPrivateKey;

  /**
   * Public encryption report key to use for testing.
   *
   * @var string
   */
  protected $testPublicKey;

  /**
   * Sets up email tests.
   */
  protected function setUp() : void {
    parent::setUp();

    $this->enableModules(['archimedes_client', 'user']);
    $this->installConfig('archimedes_client');

    // Archimedes checks the users table via SQL.
    $this->installEntitySchema('user');

    $this->testPublicKey = $this->getFixtureKey();
    $this->config('archimedes_client.settings')
      ->set('crypto.pubkey', $this->testPublicKey)->save();
  }

  /**
   * Gets test fixture key.
   */
  protected function getFixtureKey() {
    $currentDirectory = dirname(__FILE__);
    return file_get_contents($currentDirectory . '/../../fixtures/public.pem');
  }

}
