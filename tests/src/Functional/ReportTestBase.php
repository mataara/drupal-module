<?php

declare(strict_types=1);

namespace Drupal\Tests\archimedes_client\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * ReportTestBase.
 *
 * Abstract class to use for Report testing in Functional tests.
 */
abstract class ReportTestBase extends BrowserTestBase {

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = ['archimedes_client'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Private encryption report key to use for testing.
   *
   * @var string
   */
  protected $testPrivateKey;

  /**
   * Public encryption report key to use for testing.
   *
   * @var string
   */
  protected $testPublicKey;

  /**
   * Setup for report tests.
   */
  protected function setUp() : void {
    parent::setUp();
    $this->testPublicKey = $this->getFixtureKey();
    $this->config('archimedes_client.settings')->set('crypto.pubkey', $this->testPublicKey)->save();
    $this->report = \Drupal::service('archimedes_client.report');
    $this->report->regenerate();
  }

  /**
   * Obtain a public key from fixture.
   *
   * @return false|string
   *   The used fixture key.
   */
  protected function getFixtureKey() {
    $currentDirectory = dirname(__FILE__);
    return file_get_contents($currentDirectory . '/../../fixtures/public.pem');
  }

}
