<?php

declare(strict_types=1);

namespace Drupal\Tests\archimedes_client\Functional;

/**
 * ReportTest.
 *
 * Tests for Report manipulation before sending.
 */
class ReportTest extends ReportTestBase {

  /**
   * Test fixture key loads.
   */
  public function testFixtureKeyLoads(): void {
    $pubkey_data = \Drupal::config('archimedes_client.settings')->get('crypto.pubkey');
    $pubkey = openssl_pkey_get_public($pubkey_data);
    $this->assertNotFalse($pubkey);
  }

  /**
   * Tests encryption for report output.
   */
  public function testCanEncryptBlob(): void {
    $pubkey_data = \Drupal::config('archimedes_client.settings')->get('crypto.pubkey');
    $this->assertEquals($pubkey_data, $this->testPublicKey);

    $message = $this->report->getEncrypted();
    $this->assertNotNull($message);
  }

}
