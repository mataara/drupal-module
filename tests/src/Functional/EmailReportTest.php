<?php

declare(strict_types=1);

namespace Drupal\Tests\archimedes_client\Functional;

use Drupal\Core\Test\AssertMailTrait;

/**
 * EmailReportTest.
 *
 * Tests for a Mataara Report being sent over Email.
 */
class EmailReportTest extends ReportTestBase {

  use AssertMailTrait;

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = ['archimedes_client', 'mail_html_test'];

  /**
   * Tests that an email is sent correctly.
   */
  public function testSendingEmail(): void {
    // https://api.drupal.org/api/drupal/includes%21mail.inc/function/drupal_mail/7.x
    $this->assertTrue(
      $this->report->sendEmail('test@example.com')
    );
  }

  /**
   * Tests valid MIME message when creating email.
   */
  public function testValidMimeMessage(): void {
    // Send email.
    $this->report->sendEmail('test@example.com');
    $captured_emails = $this->container->get('state')->get('system.test_mail_collector') ?: [];
    $email = end($captured_emails);
    $this->assertNotNull($email);

    // Test headers.
    $headers = [
      'MIME-Version' => '1.0',
      'Content-Type' => 'text/plain; charset=UTF-8',
      'Content-Transfer-Encoding' => '7BIT',
    ];
    $this->assertEquals($headers, $email['headers']);

    // Test body.
    $body_contents = [
      'Encrypted update attached.',
    ];
    foreach ($body_contents as &$arg) {
      $this->assertMailString('body', $arg, 1);
    }

    // Test attachments.
    $attachments = $email['params']['attachments'];
    $this->assertEquals(count($attachments), 2);
    $this->assertEquals($attachments[0]['filename'], 'ekey.enc');
    $this->assertEquals($attachments[0]['filemime'], 'application/ekey');
    $this->assertNotNull($attachments[0]['filecontent']);

    $this->assertEquals($attachments[1]['filename'], 'data.enc');
    $this->assertEquals($attachments[1]['filemime'], 'application/json');
    $this->assertNotNull($attachments[1]['filecontent']);

    // Test params.
    $this->assertEquals($email['send'], 1);
    $this->assertEquals($email['subject'], "Archimedes Update");
    $this->assertNotNull($email['to']);
    $this->assertNotNull($email['from']);
  }

}
