# Mataara Tests

## Encryption key fixtures

Based on [Mataara docs](https://gitlab.com/mataara/Mataara-Server/-/blob/master/docs/source/server/generating_keypair.rst?ref_type=heads):

```bash
cd tests/fixtures/
openssl genrsa -out private.pem 4096
openssl rsa -in private.pem -outform PEM -pubout -out public.pem
```
