# Mataara Drupal Client

A module for Drupal that communicates site, module, and security information to a [Mataara server](https://gitlab.com/mataara/mataara-server) instance.

## Requirements

Report submission requires OpenSSL's RC4 cipher method, available via the Legacy provider as of OpenSSL v3.0. You may need to [enable the Legacy provider](https://stackoverflow.com/questions/73832854/php-openssl-pkcs12-read-error0308010cdigital-envelope-routinesunsupported) in your OpenSSL configuration.

## Installation

Until this module is made available on Drupal.org, it's necessary to add a repository entry for the Gitlab repository before installing:

```
composer config repositories.mataara vcs https://gitlab.com/mataara/drupal-module.git
```

You can then install the module using Composer:

```
composer require drupal/archimedes_client
```

## Configuration

Configure `report.key` via `settings.php` or `archimedes_client.settings.yml`. This should be a unique identifier shared between all instances of a given site.

As an administrator, configure settings of Mataara at `/admin/config/archimedes_client/settings`.

| Configuration | Description |
|--|--|
| Reporting Method | Protocol to connect to Mataara. Select SMTP or HTTP and provide an email or URL. |
| Reporting Frequency | How frequently to report into Mataara. |
| Public Key | The public key of your remote Mataara instance. |

When selecting a reporting method, consider that:

- HTTP report submission requires that the reporting site is capable of making an HTTP POST request to the Mataara server endpoint. This may require configuring HTTP egress rules and/or proxy configuration in Drupal.
- Email report submission requires that Drupal is capable of sending emails with attachments. This may require configuration of an SMTP transport (consider eg [SMTP](https://drupal.org/project/smtp) or [Symfony Mailer](https://drupal.org/project/symfony_mailer)); using PHP's `mail()` transport may result in empty reports.

## Formerly "Archimedes"

Please note that the source code and other documentation in this repository still refers to the project by its previous name of Archimedes.

## Coding standards

Uses standard D8 coding standards. Use Coder and phpcs to check your code: https://www.drupal.org/docs/8/modules/code-review-module/installing-coder-sniffer

Then:

```
phpcs --standard=Drupal archimedes_client.module src
```
