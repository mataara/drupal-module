<?php

namespace Drupal\archimedes_client\Item;

use Drupal\archimedes_client\Item;

/**
 * ServerHostname.
 *
 * Hostname of the server this instance of Drupal is running on.
 *
 * @package Archimedes
 * @subpackage Client
 */
class ServerHostname extends Item {

  /**
   * Gets the machien hostname using PHPs gethostname()
   *
   * @return string
   *   Hostname
   */
  public function get() {
    return gethostname();
  }

}
