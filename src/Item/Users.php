<?php

namespace Drupal\archimedes_client\Item;

use Drupal\archimedes_client\Item;

/**
 * Users.
 *
 * The number of site users currently configured.
 *
 * @package Archimedes
 * @subpackage Client
 */
class Users extends Item {

  /**
   * Gets the count of site users.
   *
   * @return int
   *   Count
   */
  public function get() {
    return intval(\Drupal::entityQuery('user')->condition('uid', 0, '!=')->count()->accessCheck(FALSE)->execute());
  }

}
