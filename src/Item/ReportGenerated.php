<?php

namespace Drupal\archimedes_client\Item;

use Drupal\archimedes_client\Item;

/**
 * ReportGenerated.
 *
 * Generation time of the report.
 *
 * @package Archimedes
 * @subpackage Client
 */
class ReportGenerated extends Item {

  /**
   * Gets the generation time.
   *
   * @return int
   *   Generation time as a UNIX timestamp
   */
  public function get() {
    return time();
  }

  /**
   * Gets the generation time formatted to RFC 2822.
   *
   * @return string
   *   HTML markup
   */
  public function render() {
    return date('r', $this->get());
  }

}
