<?php

namespace Drupal\archimedes_client\Item;

use Drupal\archimedes_client\Item;

/**
 * SiteTitle.
 *
 * The title of this site - i.e. the Drupal site name.
 *
 * @package Archimedes
 * @subpackage Client
 */
class SiteTitle extends Item {

  /**
   * Gets the site name.
   *
   * @return string
   *   Site name
   */
  public function get() {
    $config = \Drupal::config('system.site');
    return $config->get('name');
  }

}
