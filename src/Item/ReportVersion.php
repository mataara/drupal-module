<?php

namespace Drupal\archimedes_client\Item;

use Drupal\archimedes_client\Item;

/**
 * ReportVersion.
 *
 * Version of system sending the report - in this case Drupal.
 *
 * @package Archimedes
 * @subpackage Client
 */
class ReportVersion extends Item {

  /**
   * Gets the machine name of this report Version.
   *
   * @return string
   *   Report Version
   */
  public function get() {
    // Fixed value.
    return '1.0.0';
  }

  /**
   * Gets a human readable name for this report Version.
   *
   * @return string
   *   Report Version
   */
  public function render() {
    // Fixed value.
    return '1.0.0';
  }

}
