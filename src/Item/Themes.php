<?php

namespace Drupal\archimedes_client\Item;

use Drupal\archimedes_client\Item;

/**
 * Themes.
 *
 * Currently installed and enabled themes.
 *
 * @package Archimedes
 * @subpackage Client
 */
class Themes extends Item {

  /**
   * Gets an array of themes, keyed numerically.
   *
   * @return array
   *   Themes
   */
  public function get() {
    $themes = [];
    $theme_handler = \Drupal::service('theme_handler');

    foreach ($theme_handler->listInfo() as $theme => $extension) {
      $info = $extension->info;
      $themes[] = [
        'Theme'       => $theme,
        'Name'        => ($info['name'] ?? ''),
        'Description' => ($info['description'] ?? ''),
        'Version'     => ($info['version'] ?? ''),
        'Project'     => ($info['package'] ?? ''),
      // doesn't appear supported in D8 themes:
        'Url'         => '',
      ];
    }
    return $themes;
  }

  /**
   * Gets a string denoting the number of themes installed.
   *
   * @return string
   *   HTML markup
   */
  public function render() {
    $count = count($this->get());
    $p = ($count == 1) ? 'theme' : 'themes';
    return "$count $p";
  }

}
