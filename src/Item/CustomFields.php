<?php

namespace Drupal\archimedes_client\Item;

use Drupal\Core\Site\Settings;
use Drupal\archimedes_client\Item;

/**
 * Custom Fields.
 *
 * Custom Fields pre defined by the user.
 *
 * @package Archimedes
 * @subpackage Client
 */
class CustomFields extends Item {

  /**
   * Gets an array of custom fields, keyed numerically.
   *
   * @return array
   *   custom fields
   */
  public function get() {

    $field_handler = \Drupal::config('archimedes_client.settings')->get('settings.fields') ?? '';

    $parts = explode(',', $field_handler);
    $fields = [];

    foreach ($parts as $field) {
      $fields[] = [
        trim($field) => Settings::get(trim($field)),
      ];
    }

    return $fields;
  }

  /**
   * Gets a string denoting the Custom Fields.
   *
   * @return string
   *   render customs fields
   */
  public function render() {

    $field_handler = \Drupal::config('archimedes_client.settings')->get('settings.fields') ?? '';

    $parts = explode(',', $field_handler);
    $fields = " ";

    foreach ($parts as $field) {
      $fields .= " [" . trim($field) . "] ";
    }

    return $fields;
  }

}
