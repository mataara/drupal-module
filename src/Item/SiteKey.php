<?php

namespace Drupal\archimedes_client\Item;

/**
 * @file
 * Contains \Drupal\archimedes_client\Item\SiteKey.
 */

use Drupal\archimedes_client\Item;

/**
 * SiteKey.
 *
 * The random key that distinguishes this site on the Archimedes server.
 *
 * @package Archimedes
 * @subpackage Client
 */
class SiteKey extends Item {
  /**
   * Site key name in settings.
   *
   * @var string
   */
  private $keyName = 'report.key';
  /**
   * Settings object for this module.
   *
   * @var string
   */
  private $settingsName = 'archimedes_client.settings';

  /**
   * Generates a new site key.
   *
   * @return string
   *   Random 32-byte MD5 hash.
   */
  public static function generate() {
    return md5(time() . mt_rand(1000, 1000000));
  }

  /**
   * Gets the site key.
   *
   * @return string
   *   Site key
   */
  public function get() {
    $config = \Drupal::config($this->settingsName);
    $key = $config->get($this->keyName);
    if (empty($key)) {
      // Warn on empty keys.
      $message = t("No SiteKey is set! Please ensure there is a valid '%key' value in the '%settings' configuration object.",
      [
        '%key' => $this->keyName,
        '%settings' => $this->settingsName,
      ]);

      \Drupal::logger('archimedes_client')->notice($message);
    }
    elseif (!preg_match('/^[a-f0-9]{32}$/', $key)) {
      // Warn on invalid MD5 keys.
      $message = t("Invalid MD5 key! Please ensure the '%key' value in '%settings' is a valid 32 byte MD5 hash.",
      [
        '%key' => $this->keyName,
        '%settings' => $this->settingsName,
      ]);

      \Drupal::logger('archimedes_client')->notice($message);
    }

    return $key;
  }

}
