<?php

namespace Drupal\archimedes_client\Commands;

use Consolidation\AnnotatedCommand\CommandData;
use Consolidation\OutputFormatters\StructuredData\RowsOfFields;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\archimedes_client\Report;
use Drush\Commands\DrushCommands;

/**
 * Drush commands to execute Archimedes tasks from commandline.
 */
class ArchimedesClientCommands extends DrushCommands {

  /**
   * Module configuration identifier.
   */
  const CONFIG = 'archimedes_client.settings';

  /**
   * Module configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $moduleConfig;

  /**
   * Report service.
   *
   * @var \Drupal\archimedes_client\Report
   */
  protected $report;

  /**
   * ArchimedesClientCommands constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Configuration object factory.
   * @param \Drupal\archimedes_client\Report $report
   *   Archimedes report service.
   */
  public function __construct(ConfigFactoryInterface $configFactory, Report $report) {
    parent::__construct();
    $this->moduleConfig = $configFactory->get(self::CONFIG);
    $this->report = $report;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('config.factory'),
      $container->get('archimedes_client.report')
    );
  }

  /**
   * Display an Archimedes Client report.
   *
   * @option format One of: table (default), json, json-pretty, pretty-json, encrypted, encrypted-json, ejson.
   * @usage drush archimedes:report
   *   Display a report.
   * @usage drush arch-report --format=json
   *   Display a report in JSON format.
   * @usage drush arch-report --format=encrypted > report.enc
   *   Save an encypted report to file.
   * @table-style default
   * @field-labels
   *   item: Item
   *   value: Value
   *
   * @return null|\Consolidation\OutputFormatters\StructuredData\RowsOfFields
   *   Output as an array structure, or null if output written.
   *
   * @command archimedes:report
   * @aliases arch-report,arch:report
   */
  public function report($options = ['format' => 'table']) {
    $rows = [];
    switch ($options['format']) {
      case 'table':

      default:
        $data = $this->report->getRendered();
        foreach ($data as $item => $value) {
          $rows[] = [
            'item' => $item,
            'value' => $value,
          ];
        }
        break;

      case 'json':
        $this->output()->writeln($this->report->getJson());
        return;

      case 'json-pretty':
      case 'pretty-json':
        $this->output()->writeln($this->report->getJson(TRUE));
        return;

      case 'encrypted':
      case 'encrypted-json':
      case 'ejson':
        $this->output()->writeln($this->report->getEncrypted());
        return;
    }

    $result = new RowsOfFields($rows);
    return $result;
  }

  /**
   * Send an Archimedes Client report.
   *
   * @option method
   *   Reporting method to use (e.g. Email or HTTP).
   * @option location
   *   Email address or URL to send the report to (depending on chosen method).
   * @usage drush arch-send
   *   Send a report using the configured method.
   * @usage drush arch-send --method=http
   *   Send a report using HTTP.
   *
   * @command archimedes:send
   * @aliases arch-send,arch:send
   */
  public function send(array $options = ['method' => NULL, 'location' => NULL]) {
    if (empty($options['method'])) {
      $options['method'] = $this->moduleConfig->get('server.method');
    }

    if (empty($options['location'])) {
      switch ($options['method']) {
        case 'email':
          $options['location'] = $this->moduleConfig->get('server.email');
          break;

        case 'http':
          $options['location'] = $this->moduleConfig->get('server.url');
          break;
      }
    }

    $this->output()->writeln(dt('Sending "{method}" report to "{location}"...', $options));

    $status = $this->report->send($options['method'], $options['location']);

    if ($status === TRUE) {
      $this->output()->writeln('Report successfully sent.');
    }
    else {
      $this->logger()->error('Could not send report via "{method}" method. Reason: {msg}.',
        [
          'method' => $options['method'],
          'msg' => $status,
        ]
      );
    }
  }

  /**
   * Validate archimedes:send command.
   *
   * @hook validate archimedes:send
   */
  public function validateArchimedesSend(CommandData $commandData) {
    if (empty($this->input->getOption('uri'))) {
      throw new \Exception('Please specify valid hostname for this site( [-l|--uri URI] ).');
    }

    $method = $this->input->getOption('method');
    if (!empty($method) && !in_array($method, ['email', 'http'], TRUE)) {
      throw new \Exception('Unsupported method.');
    }
  }

}
