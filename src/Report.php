<?php

namespace Drupal\archimedes_client;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Extension\ExtensionPathResolver;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\State\StateInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Report.
 *
 * A report to be prepared and sent to an Archimedes server endpoint.
 *
 * @package Archimedes
 * @subpackage Client
 */
class Report implements ContainerInjectionInterface {

  /**
   * Loaded ArchimedesItem classes.
   *
   * @var array
   */
  private $items = [];

  /**
   * A list of the rendered report items.
   *
   * @var array
   */
  private $rendered;

  /**
   * A list of the report item machine values.
   *
   * @var array
   */
  private $data;

  /**
   * Base64 encoded string of the encrypted report data.
   *
   * @var string
   */
  public $encrypted = '';

  /**
   * Base64 encoded string of the encrypted ekey.
   *
   * @var string
   */
  public $ekey = '';

  /**
   * Drupal HTTP Client.
   *
   * @var GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Drupal config factory.
   *
   * @var Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * File system service.
   *
   * @var Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Drupal logger factory.
   *
   * @var Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Drupal language manager.
   *
   * @var Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Drupal time.
   *
   * @var Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Drupal state.
   *
   * @var Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Drupal mail manager.
   *
   * @var Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;

  /**
   * Report constructor.
   */
  public function __construct(ClientInterface $httpClient, ConfigFactoryInterface $configFactory, FileSystemInterface $fileSystem, LoggerChannelFactoryInterface $loggerFactory, ExtensionPathResolver $extensionPathResolver, StateInterface $state, TimeInterface $time, LanguageManagerInterface $languageManager, MailManagerInterface $mailManager) {
    $this->httpClient = $httpClient;
    $this->configFactory = $configFactory;
    $this->fileSystem = $fileSystem;
    $this->logger = $loggerFactory->get('archimedes_client');
    $this->extensionPathResolver = $extensionPathResolver;
    $this->state = $state;
    $this->time = $time;
    $this->languageManager = $languageManager;
    $this->mailManager = $mailManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('http_client'),
      $container->get('config.factory'),
      $container->get('file_system'),
      $container->get('logger.factory'),
      $container->get('extension.path.resolver'),
      $container->get('state'),
      $container->get('datetime.time'),
      $container->get('language_manager'),
      $container->get('plugin.manager.mail')
    );
  }

  /**
   * Dynamically loads an array of report item classes.
   *
   * Fetches classes from the '/lib/items' subdirectory. All item classes should
   * extend from the ArchimedesItem class.
   *
   * The 'archimedes_load_item_classfiles' invocation expects that zero or more
   * modules will return an array of individual arrays with the following keys:
   * - module : the name of the calling module
   * - subdir : a subdirectory where ArchimedesItem class files can be found
   * - files : an array of file hooks from file_scan_directory()
   *
   * NB: I don't know if there's implicit ordering for when hooks get called,
   * so modules may override each other's definitions in arbitrary order on a
   * "last come, only served" basis.
   *
   * @todo Extracting this array out into a separate class could be worthwhile.
   */
  private function createItems() {
    // Iterate over each file in the directory.
    $files = $this->getFiles();

    foreach ($files as $file) {
      // Try to load the class based on the filename.
      $class = $file->name;
      $class_namespaced = '\Drupal\archimedes_client\Item\\' . $class;

      if (class_exists($class_namespaced)) {
        $obj = new $class_namespaced();
        if ($obj instanceof Item) {
          $this->items[$class] = $obj;
        }
        else {
          $this->logger->warning("Item defined in '$class.php' doesn't extend \Drupal\archimedes_client\Item");
        }
      }
      else {
        $this->logger->warning("Item defined in '$class.php' can't be loaded by class name");
      }
    }

    // Sort the list by class name.
    ksort($this->items);
  }

  /**
   * Discover Archimedes class files.
   *
   * @return array
   *   Array of discovered classes.
   */
  private function getFiles() {
    $modulePath = $this->extensionPathResolver->getPath('module', 'archimedes_client');
    $path = DRUPAL_ROOT . '/' . $modulePath . '/src/Item';
    return $this->fileSystem->scanDirectory($path, '/\.php/');
  }

  /**
   * Fetches the raw array of items in the report.
   *
   * @return array
   *   List of each installed item.
   */
  public function getArray() {
    // Check to see if this instance has already been fetched as an array.
    if (is_array($this->data)) {
      return $this->data;
    }

    // Call the get() method of each installed Item class.
    $this->data = [];
    // Dynamically populate with available report item types.
    $this->createItems();
    // Populate data.
    foreach ($this->items as $name => $obj) {
      $this->data[$name] = $obj->get();
    }
    return $this->data;
  }

  /**
   * Fetches a JSON representation of the report.
   *
   * @param bool $pretty
   *   (optional) output pretty-printed (indented) JSON.
   *
   * @return string
   *   String of JSON encoded report
   */
  public function getJson($pretty = FALSE) {
    $options = $pretty ? JSON_PRETTY_PRINT : 0;
    return json_encode($this->getArray(), $options);
  }

  /**
   * Fetch the OpenSSL encrypted version of the report.
   *
   * @return string
   *   The encrypted report data.
   */
  public function getEncrypted() {
    $data = $this->getJson();
    return $this->encryptData($data);
  }

  /**
   * Fetch the OpenSSL envelope key for the encrypted report.
   *
   * @return string
   *   A based64-encoded version of binary key data.
   */
  public function getEkey() {
    if (empty($this->ekey)) {
      $this->getEncrypted();
    }
    return $this->ekey;
  }

  /**
   * Encrypt an input string for transmission.
   *
   * @param string $data
   *   Data string to be encrypted.
   *
   * @return string
   *   Encrypted data.
   *
   * @throws \Exception
   */
  public function encryptData(string $data) {
    $pubkey_data = $this->configFactory
      ->get('archimedes_client.settings')
      ->get('crypto.pubkey');
    $pubkey = openssl_pkey_get_public($pubkey_data);

    if ($pubkey === FALSE) {
      $err_msg = 'Error reading public key: ' . openssl_error_string();
      throw new \Exception($err_msg);
    }

    // Seal the data, generating ekeys.
    if (!openssl_seal($data, $sealed, $ekeys, [$pubkey], 'RC4')) {
      $err_msg = 'Error encrypting data: ' . openssl_error_string();
      throw new \Exception($err_msg);
    }

    // Set attributes, taking the 0th ekey, as only one provided.
    $this->ekey = $ekeys[0];
    $this->encrypted = $sealed;
    return $this->encrypted;
  }

  /**
   * Fetch rendered string representations of all report items.
   *
   * Equivalent to mapping render() onto each value returned by getArray().
   *
   * @return array
   *   An array of string-rendered ArchimedesItem objects.
   */
  public function getRendered() {
    // Check to see if this instance has already been rendered.
    if (is_array($this->rendered)) {
      return $this->rendered;
    }

    // Call the render() methods (which fallback to the get() methods)
    $this->rendered = [];
    foreach ($this->items as $name => $obj) {
      $this->rendered[$name] = $obj->render();
    }
    return $this->rendered;
  }

  /**
   * Send a report to the configured server endpoint.
   *
   * Updated the 'archimedes_client_last_report_sent' variable only if the
   * sendHttp() or sendEmail() function returns a true result.
   *
   * @param string $method
   *   Override the reporting method to be used.
   * @param string $location
   *   Override the endpoint location (e.g. Email or URL)
   */
  public function send($method = NULL, $location = NULL) {
    if (!$method) {
      $method = $this->configFactory
        ->get('archimedes_client.settings')
        ->get('server.method');
    }

    switch ($method) {
      case 'http':
        $sendResult = $this->sendHttp($location);

        if ($sendResult) {
          $this->state->set('archimedes_client.last_report', $this->time->getRequestTime());
        }

        return $sendResult;

      case 'email':
      default:
        $sendResult = $this->sendEmail($location);

        if ($sendResult) {
          $this->state->set('archimedes_client.last_report', $this->time->getRequestTime());
        }

        return $sendResult;
    }
  }

  /**
   * Invalidate existing cached item data, allowing new data to be generated.
   */
  public function invalidateCachedData() {
    $this->rendered = NULL;
    $this->data = NULL;
  }

  /**
   * Send a report to an HTTP endpoint.
   *
   * @param string $url
   *   Override the configured endpoint URL.
   */
  public function sendHttp($url = NULL) {
    // Fetch the configured URL if not passed.
    if (!$url) {
      $url = $this->configFactory
        ->get('archimedes_client.settings')
        ->get('server.url');
    }

    // Prepare POST variables.
    try {
      $data = $this->getEncrypted();
      $ekey = $this->getEkey();
    }
    catch (\Exception $e) {
      $this->logger
        ->error('Exception generating Mataara report: @message', [
          '@message' => $e->getMessage(),
        ]);
      return FALSE;
    }

    // Base64 Encode data for POST request.
    $post = [
      'ek' => base64_encode($ekey),
      'enc' => base64_encode($data),
    ];

    try {
      $response = $this->httpClient->post($url, ['form_params' => $post]);
    }
    catch (RequestException $e) {
      $this->logger->error('Exception reporting to Mataara server: @message', [
        '@message' => $e->getMessage(),
      ]);
      return FALSE;
    }

    // Check the status code is 200 (OK)
    $status = $response->getStatusCode();
    if ($status != 200) {
      $this->logger->error('HTTP @status response reporting to Mataara server.', [
        '@status' => $status,
      ]);
      return FALSE;
    }

    // Decode the JSON response.
    $data = json_decode($response->getBody());
    if (json_last_error() != JSON_ERROR_NONE || !isset($data->success)) {
      $this->logger->error('Unable to decode response from Mataara server. Response was: %body', [
        '%body' => $response->getBody(),
      ]);
      return FALSE;
    }

    // Return TRUE or an error string.
    if ($data->success !== TRUE) {
      $this->logger->error('Error response from Mataara server: %error.', [
        '%error' => $data->error,
      ]);
      return FALSE;
    }
    else {
      $this->logger->notice('Report submitted to Mataara server.');
      return TRUE;
    }
  }

  /**
   * Send a report to an Email endpoint.
   *
   * @param string $email
   *   Override the configured endpoint email address.
   */
  public function sendEmail($email = NULL) {
    $language_interface = $this->languageManager->getCurrentLanguage();

    // Fetch the configured URL if not passed.
    if (!$email) {
      $email = $this->configFactory
        ->get('archimedes_client.settings')
        ->get('server.email');
    }

    // Get encrypted data for attaching.
    try {
      $data = $this->getEncrypted();
      $ekey = $this->getEkey();
    }
    catch (\Exception $e) {
      $this->logger->error('Error sending email: %error.', [
          '%error' => $e->getMessage(),
        ]);
      return FALSE;
    }

    $params = [
      'data' => $data,
      'ekey' => $ekey,
    ];

    $mail = $this->mailManager->mail(
      'archimedes_client',
      'report',
      $email,
      $language_interface->getId(),
      $params,
    );
    return $mail['result'];
  }

}
