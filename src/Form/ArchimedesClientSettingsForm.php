<?php

namespace Drupal\archimedes_client\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * @file
 * Contains \Drupal\archimedes_client\Form\ArchimedesClientSettingsForm.
 */

/**
 * Defines a form to configure maintenance settings for this site.
 */
class ArchimedesClientSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'archimedes_client_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return ['archimedes_client.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('archimedes_client.settings');

    $smtp_link = Link::fromTextAndUrl('SMTP', Url::fromUri('https://drupal.org/project/smtp'))->toString();
    $symfony_mailer_link = Link::fromTextAndUrl('Symfony Mailer', Url::fromUri('https://drupal.org/project/symfony_mailer'))->toString();
    $email_help = $this->t('Email reporting requires that Drupal is capable of sending emails with attachments, typically requiring use of modules such as @smtp, or @symfony_mailer configured with an attachment-capable transport.', [
      '@smtp' => $smtp_link,
      '@symfony_mailer' => $symfony_mailer_link,
    ]);
    $http_help = $this->t('HTTP reporting method requires that this site can reach the Mataara server over HTTP, which may require proxy or egress rule configuration.');

    $form['report_method'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Report submission'),
      '#description' => $this->t('Archimedes can be configured to send reports to the server over different channels. Reports can be emailed as attachments to a mailbox monitored by the server, or posted directly to the server over HTTP. All reporting methods are encrypted.'),
      '#collabsible' => FALSE,
      '#collapsed' => FALSE,
    ];
    $form['report_method']['archimedes_client_server_method'] = [
      '#type' => 'radios',
      '#title' => $this->t('Method'),
      '#default_value' => $config->get('server.method'),
      '#options' => [
        'email' => $this->t('Email'),
        'http' => $this->t('HTTP'),
      ],
      '#description' => $email_help . '<br><br>' . $http_help,
    ];
    $form['report_method']['archimedes_client_server_email'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Submission email'),
      '#default_value' => $config->get('server.email'),
      '#description' => $this->t('Email address of a mailbox monitored by the Archimedes server.'),
      /* Only visible when 'method' set to Email */
      '#states' => [
        'visible' => [
          ':input[name=archimedes_client_server_method]' => ['value' => 'email'],
        ],
      ],
    ];
    $form['report_method']['archimedes_client_server_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Submission endpoint'),
      '#default_value' => $config->get('server.url'),
      '#description' => $this->t("URL of the Archimedes server's HTTP report endpoint."),

      /* Only visible when 'method' set to HTTP */
      '#states' => [
        'visible' => [
          ':input[name=archimedes_client_server_method]' => ['value' => 'http'],
        ],
      ],
    ];
    $form['report_frequency'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Reporting Frequency'),
      '#description' => $this->t("Reports are sent to the server periodically, trigged by Drupal's cron runs. On each cron run, Archimedes will check to see if the interval since the last report exceeds this configured interval, and if so it will send the next report."),
      '#collabsible' => FALSE,
      '#collapsed' => FALSE,
    ];
    $form['report_frequency']['archimedes_client_cron_interval'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Reporting Interval (seconds)'),
      '#description' => $this->t("Number of seconds between Archimedes reports. This is lower bound by the frequency Drupal's cron is run"),
      '#size' => 5,
      '#default_value' => $config->get('cron.interval'),
    ];

    $form['security'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Security'),
      '#description' => $this->t('Archimedes uses a public/private key pair to encrypt data during transmission, and expiry times to defend against replay attacks.'),
      '#collabsible' => FALSE,
      '#collapsed' => FALSE,
    ];
    $form['security']['archimedes_client_crypto_pubkey'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Public Key'),
      '#default_value' => $config->get('crypto.pubkey'),
      '#description' => $this->t('Public key used to encrypt reports.'),
      '#element_validate' => [[get_class($this), 'pubkeyValidate']],
    ];
    $form['security']['archimedes_client_report_expiry'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Expiry time (seconds)'),
      '#default_value' => $config->get('report.expiry'),
      '#size' => 5,
      '#description' => $this->t('Allow sufficient time for server to receive and process report.'),
    ];

    $form['customization'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Customizations'),
      '#description' => $this->t('Add custom system information to include with the standard reports.'),
      '#collabsible' => FALSE,
      '#collapsed' => FALSE,
    ];
    $form['customization']['archimedes_client_custom_fields'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Custom Fields'),
      '#default_value' => $config->get('settings.fields'),
      '#description' => $this->t('Fill with the name of a Setting, based on settings values from the settings.php file, separated with a comma.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Ensure openssl can parse public key field.
   */
  public static function pubkeyValidate(&$element, FormStateInterface $form_state) {
    $key = $element['#value'] ?? $element['#default_value'];
    if (!empty($key) && !openssl_pkey_get_public($key)) {
      $form_state->setError($element, $this->t('The public key you provided is invalid. Reason: %errmsg', ['%errmsg' => openssl_error_string()]));
    }
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('archimedes_client.settings')
      ->set('server.method', $form_state->getValue('archimedes_client_server_method'))
      ->set('server.email', $form_state->getValue('archimedes_client_server_email'))
      ->set('server.url', $form_state->getValue('archimedes_client_server_url'))
      ->set('cron.interval', $form_state->getValue('archimedes_client_cron_interval'))
      ->set('crypto.pubkey', $form_state->getValue('archimedes_client_crypto_pubkey'))
      ->set('report.expiry', $form_state->getValue('archimedes_client_report_expiry'))
      ->set('settings.fields', $form_state->getValue('archimedes_client_custom_fields'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
